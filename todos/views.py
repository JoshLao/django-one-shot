from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, ItemForm
# Create your views here.


def todolist_list(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists
    }
    return render(request, "todos/todos.html", context)


def todo_list_detail(request, id):
    todo = TodoList.objects.get(id=id)
    context = {
        "todo": todo,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()
    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=todo)
    context = {
        "todo_object": todo,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm()
    context = {
        "form": form
    }
    return render(request, "todos/create_item.html", context)


def todo_item_update(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm(instance=item)
    context = {
        "item_object": item,
        "form": form,
    }
    return render(request, "todos/edit_item.html", context)
